import React, { Component } from 'react';
import Person from './Person/Person'
import './App.css';

class App extends Component {

  state = {
    persons: [
      {name: "Mark", age: 37},
      {name: "Admit", age: 33},
      {name: "Alex", age: 39}
    ]
  }

  switchNameHandler = (firstName) => {
    this.setState({
      persons: [
        {name: firstName, age: 37},
        {name: "Admit", age: 33},
        {name: "Alex", age: 40}
      ]
    })
  }

  nameChangedHandler = (event) => {
    this.setState({
      persons: [
        {name: "Mark", age: 37},
        {name: event.target.value, age: 33},
        {name: "Alex", age: 40}
      ]
    })
  }
  render() {
    const style= {
      backgroundColor: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer'
    };
    return (
      <div className="App">
        <h1>Hi, I'm a React app</h1>
        <p>This is realy working!</p>
        <button 
          style={style}
          onClick={ () => this.switchNameHandler("MarkiMark3")}>Switch Name</button>
        <Person 
          name={this.state.persons[0].name} 
          age={this.state.persons[0].age}/>
        <Person 
          name={this.state.persons[1].name} 
          age={this.state.persons[1].age}
          changed={this.nameChangedHandler}
          click={this.switchNameHandler.bind(this, "MarkiMark2")}/>
        <Person 
          name={this.state.persons[2].name} 
          age={this.state.persons[2].age} > My Hobbies: Tea
        </Person>

      </div>
    );
    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'This is a new example!!!'));
  }
}

export default App;
