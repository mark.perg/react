import React, { PureComponent } from 'react';

import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
import classes from './App.css';
import withClass from '../hoc/withClass';
import Aux from '../hoc/Auxi';

export const AuthContext = React.createContext(false);
class App extends PureComponent {
  constructor(props){
    super(props);
    console.log('[App.js] Inside constructor', props);
    this.state = {
      persons: [
        {id: 1, name: "Mark", age: 37},
        {id: 2, name: "Admit", age: 33},
        {id: 3, name: "Alex", age: 39}
      ],
      showPersons: false,
      toggelClickCounter:0
    }
  }

  componentWillMount(){
    console.log('[App.js] componentWillMount()');
  }

  componentDidMount(){
    console.log('[App.js] componentDidMount()');
  }

  componentWillReceiveProps(nextProps){
    console.log('[App.js] componentWillReceiveProps()', nextProps)
  }
  
  // shouldComponentUpdate(nextProps, nextState){
  //   console.log('[App.js] shouldComponentUpdate()', nextProps, nextState);
  //   return nextState.persons !== this.state.persons ||
  //     nextState.showPersons !== this.state.showPersons;
  // }

  componentWillUpdate(nextProps, nextState){
    console.log('[App.js] componentWillUpdate()', nextProps, nextState);
  }

  static getDerivedStateFromProps(nextProps, prevState){
    console.log('[App.js] getDerivedStateFromProps()', nextProps, prevState);
    return prevState;
  }

  getSnapshotBeforeUpdate(){
    console.log('[App.js] getSnapshotBeforeUpdate()');
  }

  componentDidUpdate(){
    console.log('[App.js] componentWillUpdate()');
  }

  // state = {
  //   persons: [
  //     {id: 1, name: "Mark", age: 37},
  //     {id: 2, name: "Admit", age: 33},
  //     {id: 3, name: "Alex", age: 39}
  //   ],
  //   showPersons: false
  // }

  switchNameHandler = (firstName) => {
    this.setState({
      persons: [
        {name: firstName, age: 37},
        {name: "Admit", age: 33},
        {name: "Alex", age: 40}
      ],
    authenticated: false
    })
  }

  changeNameHandler = (event, id) => {

    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    })

    const newPerson = {...this.state.persons[personIndex]};
    newPerson.name = event.target.value;

    const updatedPersons = [...this.state.persons];
    updatedPersons[personIndex] = newPerson;

    this.setState({
      persons: updatedPersons
    })
  }

  deletePersonHandler = (personIndex) => {
    const updatedPersons = [...this.state.persons];
    updatedPersons.splice(personIndex, 1);
    this.setState({persons: updatedPersons});
  }

  loginHandler = () => {
    this.setState({authenticated: true});
  }

  toglePersonsHandler = () => {
    const viewPersonsState = this.state.showPersons;
    this.setState((prevState, props) => {
        return {
          showPersons: !viewPersonsState,
          toggelClickCounter: prevState.toggelClickCounter + 1
        }
    })
  }
  render() {
    console.log('[App.js] Inside render()');
    let persons = null;

    if(this.state.showPersons){
      persons = <Persons 
                  persons={this.state.persons}
                  clicked={this.deletePersonHandler}
                  changed={this.changeNameHandler}/>

    }


    return (
      <Aux>
        <button onClick={() => {this.setState({showPersons: true})}}>Show Persons</button>
        <Cockpit
          title={this.props.appTitle}
          showPersons={this.state.showPersons}
          persons={this.state.persons}
          login={this.loginHandler}
          clicked={this.toglePersonsHandler}/>
          <AuthContext.Provider value={this.state.authenticated}>
            {persons}
          </AuthContext.Provider>
          
      </Aux>
    );
    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'This is a new example!!!'));
  }
}

export default withClass(App, classes.App);
