import React from 'react';

import classes from './Cockpit.css';
import Aux from '../../hoc/Auxi';

const cockpit = (props) => {
    let classesGen = [];
    let btnClass = classes.Button;
    if(props.showPersons){
        btnClass = [classes.Button, classes.Red].join(' ');
    }
    
    if(props.persons.length <= 2){
      classesGen.push(classes.red);
    }

    if(props.persons.length <= 1){
      classesGen.push(classes.bold);
    }

    return (
        <Aux>
            <h1>{props.title}</h1>
            <p className={classesGen.join(' ')}>This is realy working!</p>
            <button
            className={btnClass} 
            onClick={props.clicked}>Togle Persons</button>
            <button onClick={props.login}>Log In</button>
        </Aux>
    );
};

export default cockpit;