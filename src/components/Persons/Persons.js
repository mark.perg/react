import React, {PureComponent}from 'react';

import Person from './Person/Person';

class Persons extends PureComponent  {
    constructor(props){
        super(props);
        console.log('[Persons.js] Inside constructor', props);
        this.state = {
          persons: [
            {id: 1, name: "Mark", age: 37},
            {id: 2, name: "Admit", age: 33},
            {id: 3, name: "Alex", age: 39}
          ],
          showPersons: false
        }
        this.lastPersonRef = React.createRef();
      }
    
    componentWillMount(){
      console.log('[Persons.js] componentWillMount()');
    }
  
    componentDidMount(){
      console.log('[Persons.js] componentDidMount()');
      this.lastPersonRef.current.focus();
    }

    componentWillReceiveProps(nextProps){
      console.log('[Persons.js] componentWillReceiveProps()', nextProps)
    }
    
    // shouldComponentUpdate(nextProps, nextState){
    //   console.log('[Persons.js] shouldComponentUpdate()', nextProps, nextState);
    //   return nextProps.persons !== this.props.persons ||
    //     nextProps.changed !== this.props.changed ||
    //     nextProps.clicked !== this.props.clicked;
    // }

    componentWillUpdate(nextProps, nextState){
      console.log('[Persons.js] componentWillUpdate()', nextProps, nextState);
    }

    componentDidUpdate(){
      console.log('[Persons.js] componentWillUpdate()');
    }

    render () {
        console.log('[Persons.js] render()');
        return this.props.persons.map((perons, index) => {
            return <Person 
                ref={this.lastPersonRef}
                clicked={() => this.props.clicked(index)}
                position={index}
                name={perons.name} 
                age={perons.age}
                key={perons.id}
                changed={(event) => this.props.changed(event, perons.id)}/>
            });
    }
}


export default Persons;
