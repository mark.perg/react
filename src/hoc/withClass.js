import React, {Component} from 'react';

// const withClass = (WrappedComponent, classes) => {
//     return (props) => (
//         <div className={classes}>
//             <WrappedComponent {...props}/>
//         </div>
//     )
// }

const withClass = (WrappedComponent, classes) => {
    const WithClass =  class extends Component {
        render (){
            return (
                <div className={classes}>
                    <WrappedComponent ref={this.props.fref} {...this.props}/>
                </div>
            )
        }
    }

    return React.forwardRef((props, ref) => {
        return <WithClass {...props} fref={ref} />;
    })
}

export default withClass;